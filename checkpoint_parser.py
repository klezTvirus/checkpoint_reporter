'''
##################################################################
# Parser CheckPoint Export Files v3.4a
# Releas Note
#
#
#
#
#
##################################################################
'''

from __future__ import division
import csv
import hashlib
import sys 
from os import path
from collections import defaultdict
import re
import datetime
import numpy as np
import math as m
import random as r
import nltk
from nltk.corpus import wordnet as wn
from nltk.corpus import brown

class complex_target(object):

	def _init_(self, ip,domain):
		if not ip:
			return
		self.ip = ip
		self.domain = domain if domain else ""
		return self

class complex_sign(object):

	def _init_(self, l1, l2, l3):
		self.l1 = l1
		self.l2 = l2
		self.l3 = l3
		return self


class complex_event(object):

	def _init_(self, profile):
		self.targets = []
		self.signs = []
		self.ip = ""
		self.count = 0
		self.ips_t = 0
		self.fw_t = 0
		return self

	def set_attacker(self, ip):
		self.ip = ip if ip else ""
		return self

	def add_target(self,ip):
		if ip:
			self.targets.append(ip)
		return self

class Natural_Language_Interpreter(object):

	def _init_(self, _list):
		self.ALPHA = 0.2
		self.BETA = 0.45
		self.ETA = 0.4
		self.PHI = 0.2
		self.DELTA = 0.85
		self.SIMILARITY_THRESHOLDS = [0.0,0.15,0.30,0.45,0.60,0.75,0.90,1.0,10.0]

		self.brown_freqs = dict()
		self.N = 0
		self.list = _list

	def search_similar_sign(self, search):
		results = {}
		for i in range(0, len(self.SIMILARITY_THRESHOLDS)):
			results[i] = []
		for r in self.list:
			sv = self.similarity(r, search, True)
			print(sv)
			for i in range(1, len(self.SIMILARITY_THRESHOLDS)):
				if sv > self.SIMILARITY_THRESHOLDS[i-1] and sv <= self.SIMILARITY_THRESHOLDS[i]:
					results[i].append(r)
					break
		for i in range(len(self.SIMILARITY_THRESHOLDS)-1, -1, -1):
			if len(results[i]) > 0:
				return (i,results[i])

	def get_best_synset_pair(self, word_1, word_2):
	    max_sim = -1.0
	    synsets_1 = wn.synsets(word_1)
	    synsets_2 = wn.synsets(word_2)
	    if len(synsets_1) == 0 or len(synsets_2) == 0:
	        return None, None
	    else:
	        max_sim = -1.0
	        best_pair = None, None
	        for synset_1 in synsets_1:
	            for synset_2 in synsets_2:
	               sim = wn.path_similarity(synset_1, synset_2) if wn.path_similarity(synset_1, synset_2) else 0.0
	               if sim > max_sim:
	                   max_sim = sim
	                   best_pair = synset_1, synset_2
	        return best_pair
	        
	def length_dist(self, synset_1, synset_2):
	    """
	    Distanza ontologica
	    """
	    l_dist = sys.maxsize
	    if synset_1 is None or synset_2 is None: 
	        return 0.0
	    if synset_1 == synset_2:
	        l_dist = 0.0
	    else:
	        wset_1 = set([str(x.name()) for x in synset_1.lemmas()])        
	        wset_2 = set([str(x.name()) for x in synset_2.lemmas()])
	        if len(wset_1.intersection(wset_2)) > 0:
	            # overlap
	            l_dist = 1.0
	        else:
	            # shortest path
	            l_dist = synset_1.shortest_path_distance(synset_2)
	            if l_dist is None:
	                l_dist = 0.0
	    # normalizzazione
	    return m.exp(-self.ALPHA * l_dist)

	def hierarchy_dist(self, synset_1, synset_2):
	    h_dist = sys.maxsize
	    if synset_1 is None or synset_2 is None: 
	        return h_dist
	    if synset_1 == synset_2:
	        h_dist = max([x[1] for x in synset_1.hypernym_distances()])
	    else:
	        hypernyms_1 = {x[0]:x[1] for x in synset_1.hypernym_distances()}
	        hypernyms_2 = {x[0]:x[1] for x in synset_2.hypernym_distances()}
	        lcs_candidates = set(hypernyms_1.keys()).intersection(
	            set(hypernyms_2.keys()))
	        if len(lcs_candidates) > 0:
	            lcs_dists = []
	            for lcs_candidate in lcs_candidates:
	                lcs_d1 = 0
	                if lcs_candidate in hypernyms_1:
	                    lcs_d1 = hypernyms_1[lcs_candidate]
	                lcs_d2 = 0
	                if lcs_candidate in hypernyms_2:
	                    lcs_d2 = hypernyms_2[lcs_candidate]
	                lcs_dists.append(max([lcs_d1, lcs_d2]))
	            h_dist = max(lcs_dists)
	        else:
	            h_dist = 0
	    return ((m.exp(self.BETA * h_dist) - m.exp(-self.BETA * h_dist)) / 
	        (m.exp(self.BETA * h_dist) + m.exp(-self.BETA * h_dist)))
	    
	def word_similarity(self, word_1, word_2):
	    synset_pair = self.get_best_synset_pair(word_1, word_2)
	    return (self.length_dist(synset_pair[0], synset_pair[1]) * self.hierarchy_dist(synset_pair[0], synset_pair[1]))

	def most_similar_word(self, word, word_set):
	    max_sim = -1.0
	    sim_word = ""
	    for ref_word in word_set:
	      sim = self.word_similarity(word, ref_word)
	      if sim > max_sim:
	          max_sim = sim
	          sim_word = ref_word
	    return sim_word, max_sim
	    
	def info_content(self, lookup_word):
	    if self.N == 0:
	        for sent in brown.sents():
	            for word in sent:
	                word = word.lower()
	                if not word in self.brown_freqs:
	                    self.brown_freqs[word] = 0
	                self.brown_freqs[word] += 1
	                self.N += 1
	    lookup_word = lookup_word.lower()
	    n = 0 if not lookup_word in self.brown_freqs else self.brown_freqs[lookup_word]
	    return 1.0 - (m.log(n + 1) / m.log(self.N + 1))
	    
	def semantic_vector(self, words, joint_words, info_content_norm):
	    """
	    Vettore semantico
	    """
	    sent_set = set(words)
	    semvec = np.zeros(len(joint_words))
	    i = 0
	    for joint_word in joint_words:
	        if joint_word in sent_set:
	            semvec[i] = 1.0
	            if info_content_norm:
	                semvec[i] = semvec[i] * m.pow(self.info_content(joint_word), 2)
	        else:
	            sim_word, max_sim = self.most_similar_word(joint_word, sent_set)
	            semvec[i] = self.PHI if max_sim > self.PHI else 0.0
	            if info_content_norm:
	                semvec[i] = semvec[i] * self.info_content(joint_word) * self.info_content(sim_word)
	        i = i + 1
	    return semvec                
	            
	def semantic_similarity(self, sentence_1, sentence_2, info_content_norm):
	    words_1 = nltk.word_tokenize(sentence_1)
	    words_2 = nltk.word_tokenize(sentence_2)
	    joint_words = set(words_1).union(set(words_2))
	    vec_1 = self.semantic_vector(words_1, joint_words, info_content_norm)
	    vec_2 = self.semantic_vector(words_2, joint_words, info_content_norm)
	    return np.dot(vec_1, vec_2.T) / (np.linalg.norm(vec_1) * np.linalg.norm(vec_2))


	def word_order_vector(self, words, joint_words, windex):
	    wovec = np.zeros(len(joint_words))
	    i = 0
	    wordset = set(words)
	    for joint_word in joint_words:
	        if joint_word in wordset:
	            wovec[i] = windex[joint_word]
	        else:
	            sim_word, max_sim = self.most_similar_word(joint_word, wordset)
	            if max_sim > self.ETA:
	                wovec[i] = windex[sim_word]
	            else:
	                wovec[i] = 0
	        i = i + 1
	    return wovec

	def word_order_similarity(self, sentence_1, sentence_2):
	    words_1 = nltk.word_tokenize(sentence_1)
	    words_2 = nltk.word_tokenize(sentence_2)
	    joint_words = list(set(words_1).union(set(words_2)))
	    windex = {x[1]: x[0] for x in enumerate(joint_words)}
	    r1 = self.word_order_vector(words_1, joint_words, windex)
	    r2 = self.word_order_vector(words_2, joint_words, windex)
	    return 1.0 - (np.linalg.norm(r1 - r2) / np.linalg.norm(r1 + r2))

	def similarity(self, sentence_1, sentence_2, info_content_norm):
	    return self.DELTA * self.semantic_similarity(sentence_1, sentence_2, info_content_norm) + (1.0 - self.DELTA) * self.word_order_similarity(sentence_1, sentence_2)		

class Checkpoint_parser(object):
	
	def _init_(self, input_file, openfiles=False, output_file=None):
		
		self.complex_events = {}
		self.ip2domain={}
		self.input_file=input_file
		suffix = input_file.replace(".csv", "")
		#Load Config 
		self.load_static_config()
		self.path = path.dirname(input_file)
		#Opening files
		if openfiles:
			self.open_files(output_file)
		#Whitelist loading...
		self.load_whitelist()		
		#Blacklist loading...
		self.load_blacklist()
		self.load_countries()
		self.sums = {"Generic":0, "Targeted":0, "Blacklisted":0, "Normal":0, "FalsePos":0,"Total Alarms":0,"Total Ignore":0,"Ratio":0.00,"Total Events":0}
		
		self.load_rules()
		self.ip_score = {}
		self.profile = { "SRC": {}, "DST": {}}
		self.nltk = Natural_Language_Interpreter()
		_list = self.generate_l4()
		self.nltk._init_(_list)
		self.record = self.generate_matrix(False)



	def load_static_config(self, suffix=""):
		self.config = {
			"rules" : {
				"1st": "./lib/cp_parser/config/rules/l1rules.csv",
				"2nd": "./lib/cp_parser/config/rules/l2rules.csv",
				"3rd": "./lib/cp_parser/config/rules/l3rules.csv"
			},
			"lists" : {
				"black": "./lib/cp_parser/config/blacklist.csv",
				"white": "./lib/cp_parser/config/whitelist.csv",
				"countries" : "./lib/cp_parser/config/countries.csv"
			},
			"patterns": {
				"ip": r'([\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})',
				"domain": r'(?!\([\w\.]*\))(([a-zA-Z\.]*)?(\_*)([\w\.\-]*))',
				"service_port": r"\w*\/\d+"
			},
			"output":{
				"summary": "./lib/cp_parser/middleproduct/summary" + "_" + suffix + ".csv",
				"details": "./lib/cp_parser/middleproduct/details" + "_" + suffix + ".csv",
				"verbose_details": "./lib/cp_parser/middleproduct/def_details" + "_" + suffix + ".csv",
				"bydest": "./lib/cp_parser/middleproduct/bydest" + "_" + suffix + ".csv",
				"byevents": "./lib/cp_parser/middleproduct/byevent" + "_" + suffix + ".csv",
				"parsed_data": "./lib/cp_parser/middleproduct/data" + "_" + suffix + ".csv"

			}
		}

	def open_files(self, output_file=None):
		if not output_file == None:
			o = open(output_file, 'w')
		else:
			o = open(self.config["output"]["summary"], 'w')
		self.files = {
			"summary" : o
			# Deprecated
			#,
			#"details" : open(self.config["output"]["details"],'w'),
			#"verbose_details" : open(self.config["output"]["verbose_details"],'w'),
			#"top_destinations" : open(self.config["output"]["bydest"],'w'),
			#"top_events" : open(self.config["output"]["byevents"],'w')
		}

	def close_files(self):
		for k,v in self.files.items():
			v.close()
	
	def load_whitelist(self):
		self.whitelist = []
		with open(self.config["lists"]["white"], 'r') as wl:
			wlreader = csv.reader(wl, delimiter=";")
			for row in wlreader:
				self.whitelist.append(row[0])

	def load_blacklist(self):
		self.blacklist = []
		with open(self.config["lists"]["black"], 'r') as wl:
			blreader = csv.reader(wl, delimiter=";")
			for row in blreader:
				self.blacklist.append(row[0])

	def load_countries(self):
		with open(self.config["lists"]["countries"], 'r') as config:
			#Loading configurations...
			configreader = csv.reader(config, delimiter=';')
			confighead = next(configreader, None)
			column = {}
			dictr = defaultdict(list)
			for h in confighead:
				column[h] = []
			for row in configreader:
				for h,v in zip(confighead, row):
					column[h].append(v)
				dictr[row[1]] = row[3]
		self.countryvalues = dictr

	def load_rules(self):
		
		rules = {}
		for r in ["L1", "L2", "L3"]:
			rules[r] = {}


		with open(self.config["rules"]["1st"], 'r') as config:
			#Loading configurations...
			configreader = csv.reader(config, delimiter=';')
			confighead = next(configreader,None)
			column = {}
			dictr = defaultdict(list)
			for h in confighead:
				column[h] = []
			for row in configreader:
				for h,v in zip(confighead, row):
					column[h].append(v)
				dictr[row[0]] = row[1]
			rules["L1"] = dictr
		with open(self.config["rules"]["2nd"], 'r') as config:
			#Loading configurations...
			configreader = csv.reader(config, delimiter=';')
			confighead = next(configreader,None)
			column = {}
			dictr = defaultdict(list)
			for h in confighead:
				column[h] = []
			for row in configreader:
				for h,v in zip(confighead, row):
					column[h].append(v)
				dictr[row[0]] = [float(row[1].replace(",", ".")), float(row[2].replace(",", ".")),float(row[3].replace(",", ".")), float(row[4].replace(",", "."))]
			rules["L2"] = dictr
		with open(self.config["rules"]["3rd"], 'r') as config:
			#Loading configurations...
			configreader = csv.reader(config, delimiter=';')
			confighead = next(configreader,None)
			column = {}
			dictr = defaultdict(list)
			for h in confighead:
				column[h] = []
			for row in configreader:
				for h,v in zip(confighead, row):
					column[h].append(v)
				dictr[row[0]] = [float(row[1].replace(",", ".")), float(row[2].replace(",", "."))]
			rules["L3"] = dictr
		rules["L4"] = {}
		self.db_rules = rules
		return rules

	def generate_l4(self):
		_list = [] 
		for i in [ x for x in self.db_rules["L3"].keys() ]:
			sign1, sign2 = self.construct_subsign(i)
			_list.append(sign1)
			self.db_rules["L4"][sign1] = self.db_rules["L3"][i]
			if sign1 != sign2:
				_list.append(sign2)
				self.db_rules["L4"][sign2] = self.db_rules["L3"][i]
		return _list
	
	def construct_subsign(self, sign):
		sign1 = re.sub(r'\([\w|\W]*\)', '', sign)
		subsign = re.sub(r'\s*\-\s\w{3}\s\d*\W*', '', sign1)
		j = str(subsign).split(" ")
		j_length = len(j)
		if j_length <= 3:
			return (subsign, subsign)
		else:
			return (" ".join(j[:3])," ".join(j[-3:]))

	def get_summary(self):
		return self.sums

	def print_summary(self,flat=True):
		if flat:
			print(self.sums)
		else:
			for k,v in self.sums.items():
				print (k + ": " + str(v))
	def write_summary(self):
		headers = ";".join(self.sums.keys()) + "\n"
		values = ""
		self.files["summary"].write(headers)
		for k,v in self.sums.items():
			values += v + ";"
		self.files["summary"].write(values[:-1])

	def finalize(self):
		print ("Generating Summary...    ")
		self.sums["Total Alarms"] = self.sums["Generic"] + self.sums["Targeted"]
		self.sums["Total Events"] = self.sums["Generic"] + self.sums["Targeted"] + self.sums["FalsePos"] + self.sums["Blacklisted"] + self.sums["Normal"]
		self.sums["Total Ignore"] = self.sums["FalsePos"] + self.sums["Blacklisted"] + self.sums["Normal"]
		self.sums["Ratio"] = float(self.sums["Total Alarms"])/30.0
		print ("... Done")

	def add_sign(self,sign):
		if sign == None:
			print("NO SIGN ENTERED\n")
			return
		print("Nuova firma trovata: " + sign + " \nAnalisi semantica in corso...    ")
		to_search1,to_search2 = self.construct_subsign(sign)
		#print("Original: " + sign + "\n Search A: " + to_search1 + "\n Search B: " + to_search2 + "\n")
		sv1 = self.nltk.search_similar_sign(to_search1)
		sv2 = self.nltk.search_similar_sign(to_search2)
		similarities = sv1[1] if sv1[0] > sv2[0] or (sv1[0] == sv2[0] and len(sv1) > len(sv2)) else sv2[1]
		length = len(similarities)
		values = [0.0,0.0]
		for s in similarities:
			#print(" Cutted: " + s)
			values[0] += float(self.db_rules["L4"][s][0])
			values[1] += float(self.db_rules["L4"][s][1])
			
		values[0] /= m.ceil(float(length))
		values[0] = int(m.ceil(values[0]))
		values[1] /= float(length)
		#print(values)
		self.db_rules["L3"][sign] = values
		print("...Done\n")
		print("Inserimento nel db...    ")
		with open(self.config["rules"]["3rd"], "a") as rules:
		    rules.write(";".join([sign] + [str(x) for x in values]) + "\n")
		print("...Firma aggiunta al db\n")


	def aggregate(self, datelist):

		day = 0
		dictime = defaultdict(list)
		for dt in datelist:
			if dt[1].day != day:
				day = dt[1].day
				dictime[day] = [dt]
			else:
				dictime[day].append(dt)
		return dictime

	def gini_factor(self, _P, _Q):
		s=0.0
		n = len(_P)
		if n != len(_Q):
			return
		for i in range(1,n):
			s += float(abs(_P[i] - _Q[i]))
		return float(2.0/float(n)*s)

	def generateSeries(self,frequencies):
		#Daily frequencies given, generates two series needed to obtain concentration index
		total = 0.0
		n = 0.0
		for f in frequencies:
			total += float(f)
			n += 1.0
		period = float(total)/float(n)		
		_P = [float(k)/float(n) for k in range(1, int(n), 1)]
		_Q = []
		k=0
		for f in sorted(frequencies):
			p = float(f)/max(float(total),1.0)
			if k == 0:
				_Q.append(p)
			elif k == n - 1:
				break
			else:
				_Q.append(_Q[k-1] + p)
			k+=1
		return _P, _Q

	def average(self, _list, square=False):
		#Calculates E[x] of the serie in _list
		#If square is true, calculates E[x^2] 
		avg = 0.0
		if square:
			for v in _list:
				avg += float(v)*float(v)	
		else:
			for v in _list:
				avg += float(v)
		return float(avg)/float(len(_list))

	def variance(self, _list, radix=False):
		#Calculate variance of the serie in _list
		avg = self.average(_list)
		avg_2 = self.average(_list,True)
		var = float(avg_2) - float(avg)*float(avg)
		return var if not radix else m.sqrt(radix)

	def calculate_timing(self,ip,_list): 
		return self.analize_frequency(ip,self.aggregate(self.parseTimelist(_list))) #if len(timeline["IPS"]) >= self.config["rules"]["L1"]["IPS"] or if len(timeline["Firewall"]) >= self.config["rules"]["L1"]["Firewall"] or if len(timeline["IPS"]) + len(timeline["Firewall"]) >= self.config["rules"]["L1"]["Generic"]  

	def parse_profile(self):
		print("Valutazione delle distribuzioni stocastiche degli eventi...    ")
		seen = []
		xyz = []
		score = {}	
		for k,v in self.profile["DST"].items():
			if len(v) > 0:
				#print("ARRIVED TO: " + k + "\n")
				self.calculate_timing(k,v)
			else:
				continue
			score[k] = { "value" : 0.0, "hashes" : [] }
			score[k]["ip"] = {}

		#print (self.ip_score.keys())
		for row in self.record:
			
			if row[1] in xyz:
				if row[3] not in score.keys():
					score[row[3]] = dict()
					score[row[3]]["ip"] = dict()
					score[row[3]]["value"] = 0.0
					score[row[3]]["hashes"] = []
					score[row[3]]["ip"].update({ row[1] : 1 })
					score[row[3]]["value"] = 0.0#(0.5)*float(1.0/float(row[8]))*float(row[2])*float(self.ip_score[row[0]])
					score[row[3]]["hashes"] = [row[0]]
					
				if row[1] not in score[row[3]]["ip"].keys():
					score[row[3]]["ip"] = dict()
					score[row[3]]["ip"][row[1]] = 1
					score[row[3]]["value"] = 0.0#(0.5)*float(1.0/float(row[8]))*float(row[2])*float(self.ip_score[row[0]])
					score[row[3]]["hashes"] = [row[0]]
				score[row[3]]["ip"][row[1]] += 1 
			else:
				if row[3] not in score.keys():
					score[row[3]] = dict()
					score[row[3]]["ip"] = dict()
					score[row[3]]["value"] = 0.0
					score[row[3]]["hashes"] = []
					score[row[3]]["ip"].update({ row[1] : 1 })
					score[row[3]]["value"] = 0.0#(0.5)*float(1.0/float(row[8]))*float(row[2])*float(self.ip_score[row[0]])
					score[row[3]]["hashes"] = [row[0]]
					
				if row[1] not in score[row[3]]["ip"].keys():
					score[row[3]]["ip"] = dict()
					score[row[3]]["ip"][row[1]] = 1
					score[row[3]]["value"] = 0.0#(0.5)*float(1.0/float(row[8]))*float(row[2])*float(self.ip_score[row[0]])
					score[row[3]]["hashes"] = [row[0]]
				score[row[3]]["ip"][row[1]] = 1
				xyz.append(row[1])
			if row[0] not in self.ip_score.keys():
				continue
			if row[3] in seen:
				score[row[3]]["value"] += (0.5)*float(1.0/float(row[8]))*float(row[2])*float(self.ip_score[row[0]])
				score[row[3]]["hashes"].append(row[0]) 
			else:
				#print ("HASH: " + "8f6bb9e72b451d41002b859b80b104c9 " + " VALUE: " + str(self.ip_score["8f6bb9e72b451d41002b859b80b104c9"]) )
				score[row[3]]["value"] = (0.5)*float(1.0/float(row[8]))*float(row[2])*float(self.ip_score[row[0]])	
				score[row[3]]["hashes"] = []
				seen.append(row[3])
		
		maxvalue = max([ y["value"] for x,y in score.items() ]) 
		for ip,val in score.items():
			
			print(ip + "   " + str(val["value"]))
			vvv = int(len(val["hashes"])/max(len(val["ip"].keys()),1.0))
			if val["value"] >0.0 and val["value"] <= maxvalue/1.8:
				self.sums["Generic"] += vvv
			elif val["value"] > maxvalue/1.8:
				self.sums["Targeted"] += vvv
			else:
				self.sums["FalsePos"] += vvv
		print("...Done")
		self.finalize()
		#print(score.items())

	def fast_discrete_fourier_transform(self, x):
		"""A vectorized, non-recursive version of the Cooley-Tukey FFT"""
		x = np.asarray(x, dtype=float)
		N = x.shape[0]

		if np.log2(N) % 1 > 0:
			raise ValueError("size of x must be a power of 2")

		# N_min here is equivalent to the stopping condition above,
		# and should be a power of 2
		N_min = min(N, 32)

		# Perform an O[N^2] DFT on all length-N_min sub-problems at once
		n = np.arange(N_min)
		k = n[:, None]
		M = np.exp(-2j * np.pi * n * k / N_min)
		X = np.dot(M, x.reshape((N_min, -1)))

		# build-up each level of the recursive calculation all at once
		while X.shape[0] < N:
			X_even = X[:, :X.shape[1] / 2]
			X_odd = X[:, X.shape[1] / 2:]
			factor = np.exp(-1j * np.pi * np.arange(X.shape[0]) / X.shape[0])[:, None]
			X = np.vstack([X_even + factor * X_odd, X_even - factor * X_odd])

		return X.ravel()

	def analize_frequency(self, ip, dictime):
		#print(dictime)
		th = 15.0
		scores = {}
		hashes = {}
		daily_scores = {}
		if len(dictime.items())==0:
			return
		for k,v in dictime.items():
			hashes[k] = []
			scores[k] = {}
			tot = 0
			frequencies  = []
			i=25
			dayf = []
			correct_sequence = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]

			for i in correct_sequence:
				frequencies.append(0)

			for d_v in v:
				hashes[k].append(d_v[0])
				d = d_v[1]

				if i != d.hour:
					i=d.hour
					frequencies[i] = len(dayf)
					tot += len(dayf)
					dayf = []
				else:
					dayf.append(d)
			#print(dayf)
			_P, _Q = self.generateSeries(frequencies)

			#print(frequencies)

			scores[k]["eventcount"] = float(tot+1)
			scores[k]["gini"] = self.gini_factor(_P,_Q)
			scores[k]["average"] = max(self.average(frequencies), 1.0)
			scores[k]["variance"] = self.variance(frequencies)
			scores[k]["std_deviation"] = self.variance(frequencies)

			#print(scores)
			daily_scores[k] = max(0.25*max(scores[k]["eventcount"]/th*scores[k]["gini"], 0.0), min(abs((scores[k]["std_deviation"]-scores[k]["average"])/scores[k]["average"]),1 ))
			#print(daily_scores[k])
		for k in daily_scores:
			for ash in hashes[k]:
				self.ip_score[ash] = daily_scores[k]
		#print (self.sums)


	def normalize(self, datestring):
		if re.match(r'^\d{2}\s\w{3}\s\d{4}\,\s\d{2}\:\d{2}(?:\:\d{2}$|$)', datestring):
			tmp = datestring.replace(":", " ").replace(",", "").split(" ")
			rstring = ":".join([tmp[3],tmp[4],"00"]) + " " + " ".join([tmp[0],tmp[1],tmp[2]])
		else:
			rstring = datestring
		return rstring 
		
	def parseDate(self, datestring):
		#print(datestring)
		rstring = self.normalize(datestring)
		#print(rstring)
		months = {"Gen":1,"Feb":2, "Mar":3,"Apr":4,"Mag":5, "Giu":6,"Lug":7,"Ago":8,"Set":9,"Ott":10,"Nov":11,"Dic":12,"Jan":1,"May":5,"Jun":6,"Jul":7,"Sep":9,"Oct":10,"Dec":12}
		arr = rstring.replace(":", " ").split(" ")
		return datetime.datetime(int(arr[5]), months[arr[4]], int(arr[3]), int(arr[0]), int(arr[1]), int(arr[2]))

	def generate_indexes(self, headers):
		indexes = defaultdict(list)
		i=0
		for h in headers:
			h = h.strip()
			if h == "":
				continue
			indexes[h] = i
			i+=1
		return indexes
	
	def validate_ip(self,ip):
		oct = str(ip).split('.')
		subnet24 = '.'.join([oct[0], oct[1], oct[2]])
		subnet16 = '.'.join([oct[0], oct[1]])
		if ip in self.blacklist:
			return False
		if ip in self.whitelist or subnet24 in self.whitelist or subnet16 in self.whitelist:
			return True

	def parseTimelist(self,timelist,sort=True,reverse=False):
		timeline = []
		for v in timelist:
			timeline.append([v[0], self.parseDate(v[1])])
		return sorted(timeline, key=lambda t : t[1], reverse=reverse) if sort else timeline

	def validate_asset(self,ip):
		m = re.findall(r"^62\.241\.\d{1,3}\.\d{1,3}$", ip)
		return True if m else False

	def get_country_value(self, countryname):

		return self.countryvalues[countryname] if self.countryvalues[countryname] != None and self.countryvalues[countryname] != [] else 3 


	def get_probabilities(self, sign):
		return self.db_rules["L2"][sign]

	def get_handicap(self, sign):
		return self.db_rules["L3"][sign]

	def randomchoice(self, _list):
		total = sum(w for w in _list) if len(_list) > 0 else 1
		_r = r.uniform(0, total)
		upto = 0
		index = 0
		for w in _list:
			if upto + w >= _r:
				return [index, _list[index]]
			upto += w
			index +=1
		assert False


	def distorcer(self, noise_ratio):
		pass

	def bisect_threshold(self):
		pass

	def generate_matrix(self, usefile=False, autoprofile=False):

		print("Generazione Matrice Eventi...    ")
		if autoprofile:
			self.profile = { "SRC" : {}, "DST" : {}}
		eol = "\n"
		paired = []
		data = open(self.config["output"]["parsed_data"], 'w')
		if usefile:
			s_headers = ["Hash", "Source", "CountryValue", "Target", "Port", "Time", "MostProbType", "Probability", "L2", "L3"]		
			data.write(";".join(s_headers) + eol)
		else:
			matrix = []

			
		with open(self.input_file, 'r') as events:
			eventreader = csv.reader(events, delimiter=',')
			eventhead = next(eventreader,None)
			
			seen = { "SRC" : [], "DST" : [] }
			totals = { "SRC" : {}, "DST" : {} }

			indexes = self.generate_indexes(eventhead);
			signs = [ x for x,y in self.db_rules["L3"].items()]

			for row in eventreader:
				md5_hash = hashlib.md5()
				#for h,v in zip(eventhead, row):
				time = row[indexes["Start Time"]]
				temp = re.findall(self.config["patterns"]["ip"], row[indexes["Source"]])
				if temp:
					ip = temp[0]
				#print str(ip)
				temp = re.findall(self.config["patterns"]["domain"], row[indexes["Source"]])
				if temp:
					srcdomain = temp[0][0]
				else:
					srcdomain = ''
				temp = re.findall(self.config["patterns"]["ip"], row[indexes["Destination"]])
				if temp:
					asset = temp[0]
				temp = re.findall(self.config["patterns"]["domain"], row[indexes["Destination"]])
				if temp:
					assetdomain = temp[0][0]
					self.ip2domain[ip] = assetdomain
				else:
					assetdomain = ''
					if self.ip2domain[ip] == None:
						self.ip2domain[ip] = ''
				if self.validate_ip(ip):
					self.sums["Normal"] += 1
					continue
				if not self.validate_asset(asset):
					self.sums["FalsePos"] += 1
					continue

				temp = re.findall(self.config["patterns"]["service_port"], row[indexes["Service"]])
				if temp:
					port = temp[0].split('/')[1]
				else:
					port = "80"
				if re.findall(r"^(25|110|143|465|587|993|995)$",port):
					#print(port);
					self.sums["Blacklisted"] += 1
					continue
				if not re.findall(r"^(80|443)$",port):
					pass

				country = row[indexes["Source Country"]]
				l1 = row[indexes["Event Name"]]
				l2 = row[indexes["Attack Name"]]
				l3 = row[indexes["Attack Information"]]
				pair = str(ip) + str(asset) + str(l3)

				if str(l3) not in signs:
					#print("NOT PRESENT\n")
					self.add_sign(str(l3))
					signs.append(str(l3))
				if str(l3) not in signs:
					pass
					#print("NOT ADDED\n")

				#if pair in paired:
				#	continue
				#else:
				#	paired.append(pair)

				data.write(";".join([ip,country,asset,port,time,l1,l2,l3]) + eol)
				#print str(column['Sign'])
				
				c_value = self.get_country_value(country)

				probabilities = self.get_probabilities(str(l2))
				
				handicap = self.get_handicap(str(l3))
				
				adjust = [ (1.0 - handicap[1])/r.random() for i in range(4) ]

				adjust[int(handicap[0])] = handicap[1]

				_sum = 0.0
				zipp = list(zip(probabilities, adjust))
				index = 0
				for a,b in zipp:
					if a == 0:
						a += b/5.0
					if b == 0:
						b += a/5.0
					_sum += a*b*100
					zipp[index] = (a,b)
					index += 1
				
				newprob = [a*b*100/_sum for a,b in list(zip(probabilities, adjust ))]

				#print(newprob)
								
				row = [str(ip),str(c_value),str(asset),str(port),str(time)]
				md5_hash.update(bytes("".join(row),"utf-8"))


				row = [md5_hash.hexdigest()] + [str(ip),str(c_value),str(asset),str(port),str(time)] + self.randomchoice(newprob)

				if ip in seen["SRC"]:	
					totals["SRC"][ip] +=1
					self.profile["SRC"][ip].append((md5_hash.hexdigest(),str(time)))
				else:
					totals["SRC"][ip] = 0
					self.profile["SRC"][ip] = []
					seen["SRC"].append(ip)

				if asset in seen["DST"]:	
					totals["DST"][asset] +=1
					self.profile["DST"][asset].append((md5_hash.hexdigest(),str(time)))
				else:
					totals["DST"][asset] = 0
					self.profile["DST"][asset] = []
					seen["DST"].append(asset)

				matrix.append(row)
			
			for row in matrix:
				row.append(totals["SRC"][row[1]] +1)


			data.close()
			self.record = matrix
			print("...Done")
			return matrix

	def parse(self):

		false_positives = 0 
		normal_activity = 0
		generic_attack = 0
		targeted_attack = 0
		blklisted_attack = 0
		
		#Type Attack Vector
		gen = []
		tgt = []
		blklist = []
		paired = []
		
		#Opening rules files...
		rules = self.load_rules()
			
		with open(self.input_file, 'r') as events:
			eventreader = csv.reader(events, delimiter=',')
			eventhead = next(eventreader,None)
			
			indexes = self.generate_indexes(eventhead);
			#print indexes
			
			tops = {}
			
			self.d.write("Type;Attacker;Asset\n")
			
			for header in ["Event", "Asset"]:
				tops[header] = {}
				
			for row in eventreader:
				#for h,v in zip(eventhead, row):
				temp = re.findall(self.ip_reg, row[indexes["Source"]])
				if temp:
					ip = temp[0]
				#print str(ip)
				temp = re.findall(self.dom_reg, row[indexes["Source"]])
				if temp:
					srcdomain = temp[0][0]
				else:
					srcdomain = ''
				temp = re.findall(self.ip_reg, row[indexes["Destination"]])
				if temp:
					asset = temp[0]
				temp = re.findall(self.dom_reg, row[indexes["Destination"]])
				if temp:
					assetdomain = temp[0][0]
				else:
					assetdomain = ''
				pair = str(ip) + str(asset)
				if pair in paired:
					continue
				else:
					paired.append(pair)

				oct = str(ip).split('.')
				sign = row[indexes["Attack Information"]]
				subnet24 = '.'.join([oct[0], oct[1], oct[2]])
				subnet16 = '.'.join([oct[0], oct[1]])
				#print str(column['Sign'])
				if self.validate_ip(ip):
					normal_activity += 1
					false_positives += 1
					#print "Found IP in Whitelist"
					continue
				if rules["L3"][sign]:
					rule = rules["L3"][sign]
					#print rule
					if int(rule) == 0:
						normal_activity += 1
					elif int(rule) == 1:
						blklisted_attack += 1
						blklist.append(sign)
						self.d.write("Blacklisted activity;" + ip + ";" + asset + "\n")
					elif int(rule) == 2:
						generic_attack += 1
						gen.append(sign)
						self.d.write("Generic attack;" + ip + ";" + asset + "\n")
					elif int(rule) == 3:
						targeted_attack += 1
						tgt.append(sign)
						self.d.write("Targeted attack;" + ip + ";" + asset + "\n")
				else:
				
					#print "Sign not found"
					print(sign)
				
				if not sign in tops["Event"]:
					tops["Event"][sign] = {"Generic": 0, "Targeted": 0, "Blacklisted": 0, "Total" : 0}
				
				if not assetdomain in tops["Asset"]:
					tops["Asset"][assetdomain] = {"Generic": 0, "Targeted": 0, "Blacklisted": 0, "Normal": 0, "Total" : 0}
				else:
					if sign in gen:
						tops["Asset"][assetdomain]["Generic"] +=1
						tops["Event"][sign]["Generic"] += 1
						
						
					elif sign in tgt: 
						tops["Asset"][assetdomain]["Targeted"] +=1
						tops["Event"][sign]["Targeted"] += 1
						
					elif sign in blklist:
						tops["Asset"][assetdomain]["Blacklisted"] +=1
						tops["Event"][sign]["Blacklisted"] += 1
						tops["Asset"][assetdomain]["Total"] -= 1
						tops["Event"][sign]["Total"] -= 1
					else:
						tops["Asset"][assetdomain]["Normal"] += 1
						tops["Asset"][assetdomain]["Total"] -= 1
						tops["Event"][sign]["Total"] -= 1
					
					tops["Asset"][assetdomain]["Total"] += 1
					tops["Event"][sign]["Total"] += 1
					
			top_events = sorted(tops["Event"].items(), key=lambda x: x[1]["Total"], reverse=False)
			top_assets = sorted(tops["Asset"].items(), key=lambda x: x[1]["Total"], reverse=False)
			
			self.byent.write('"' + "Event Name" +'"' + ";" + "Total" + ";" + "Targeted" + ";" + "Generic" + ";" + "Blacklisted" + "\n")
			self.bydst.write('"' + "Asset" +'"' + ";" + "Generic" + ";" + "Targeted" + ";" + "Blacklisted" + ";"  + "Total" + "\n")
			
			for key, value in top_events :
				self.byent.write('"' + key +'"' + ";" + str(value["Total"]) + ";" + str(value["Targeted"]) + ";" + str(value["Generic"]) + ";" + str(value["Blacklisted"]) + "\n")
			
			for key, value in top_assets :
				self.bydst.write('"' + key +'"' + ";" + str(value["Generic"]) + ";" + str(value["Targeted"]) + ";" + str(value["Blacklisted"]) + ";" + str(value["Total"]) + "\n")
			
			eol = "\n"
			total_alarms=generic_attack+targeted_attack
			total_events=false_positives+normal_activity+blklisted_attack+targeted_attack+generic_attack
			total_fpos=blklisted_attack+normal_activity+false_positives
			alarm2total_ratio=float((float(total_alarms)/float(total_events))*100.00)
			s_headers = ["Generic", "Targeted", "Blacklisted", "Normal", "FalsePos","Total Alarms","Total Ignore","Ratio","Total Events"]
			s_content = [str(generic_attack),str(targeted_attack), str(blklisted_attack),str(normal_activity),str(false_positives),str(total_alarms),str(total_fpos),str(alarm2total_ratio),str(total_events)]
			self.s.write(";".join(s_headers) + eol)
			self.s.write(";".join(s_content) + eol)
			self.s.close()
			self.d.close()
			
			summmary_dict = { s_headers[0] : s_content[0],s_headers[1] : s_content[1], s_headers[2] : s_content[2], s_headers[3] : s_content[3], s_headers[4] : s_content[4] } 
			
			return { 'summary' : summmary_dict, 'events': top_events , 'assets' : top_assets}		


a = Checkpoint_parser()
a._init_(sys.argv[1],True,sys.argv[2] if len(sys.argv) >=3 else None)
a.parse_profile()
a.print_summary(False)
#a.write_summary()
a.close_files()